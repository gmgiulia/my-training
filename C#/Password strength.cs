using System;

namespace PasswordChecker
{
  class Program
    /* this program measures the strength of any given password score is 1 to 5(weak to extremely strong)
    Author Giulia Meniconzi */
    
  {
    public static void Main(string[] args)
    { //define variables
      int minLength=9;
      string uppercase="ABCDEFGHIJKLMNOPQRSTUYXWZ";
      string lowercase="abcdefghijklmnopqrstuvwxyz";
      string digits="1234567890";
      string specialChars="`~!@#$%^&*()_-+=<>,.?:''{}[]/|";
      Console.WriteLine("Check the strength of your password!\nPlease enter your password");
      string password=Console.ReadLine();
      int score=0; 
      if(password.Length>=minLength)
      {
        score++;
      }
      if(Tools.Contains(password, uppercase))
      {
        score++;
      }
      if(Tools.Contains(password, lowercase))
      {
        score++;
      } 
      if(Tools.Contains(password, digits))
      {
        score++;
      }
      if(Tools.Contains(password, specialChars))
      {
        score++;
      }
      Console.WriteLine("Password score is "+ score);
      switch(score)
      {
        case 1:
          Console.WriteLine("The password is weak!");
          break;
        case 2:
          Console.WriteLine("The password has a medium score");
          break;
        case 3:
          Console.WriteLine("Strong score");
          break;
        case 4:
          Console.WriteLine("Extremely strong score!");
          break;
          case 5:
          Console.WriteLine("Extremely strong score!");
          break;
        default:
          Console.WriteLine("The password does not meet any of the standards");
          break;
      }

    }
  }
}
