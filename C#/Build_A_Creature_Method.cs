using System;

namespace ExquisiteCorpse
  /* In the game Exquisite Corpse, participants draw either a head, body, or feet of a creature. The players dont know how their part of the body connects to the other two, until the drawing is finished and revealed. 
  This program mimics the Exquisite Corpse game.
  Author: Giulia Meniconzi */
{
  class Program
  {
    static void Main(string[] args)
    {
      BuildACreature("ghost", "monster", "bug");
      SwitchCase(1,1,1);
      RandomMode();
      
    }
    static void BuildACreature(string head, string body, string feet)
      /* three creatures, broken up into three different parts (head, body, feet). We can call a combination of these methods to build various creatures.*/
    {
      switch (head)
      {
        case "ghost":
          GhostHead();
          break;
        case "bug":
          BugHead();
          break;
        case "monster":
          MonsterHead();
          break;
      }
      
      switch (body)
      {
        case "ghost":
          GhostBody();
          break;
        case "bug":
          BugBody();
          break;
        case "monster":
          MonsterBody();
          break;
      }
      
      switch (feet)
      {
        case "ghost":
          GhostFeet();
          break;
        case "bug":
          BugFeet();
          break;
        case "monster":
          MonsterFeet();
          break;
      }
      
      /* Rather than passing in arguments each time we want to build a creature with a method that randomly assemble a body each time the program is run */
      
    static void RandomMode()
    {
      Random randomBody = new Random();
      int head=randomBody.Next(1, 4);
      int body=randomBody.Next(1, 4);
      int feet=randomBody.Next(1, 4);
      SwitchCase(head, body, feet);
    }
      
    static void SwitchCase(int head, int body, int feet)
    {
      switch (head)
      {
        case "1":
          GhostHead();
          break;
        case "2":
          BugHead();
          break;
        case "3":
          MonsterHead();
          break;
      }
      
      switch (body)
      {
        case "1":
          GhostBody();
          break;
        case "2":
          BugBody();
          break;
        case "3":
          MonsterBody();
          break;
      }
      
       switch (feet)
      {
        case "1":
          GhostFeet();
          break;
        case "2":
          BugFeet();
          break;
        case "3":
          MonsterFeet();
          break;
      }
    }
    }

    static void GhostHead()
    {
      Console.WriteLine("     ..-..");
      Console.WriteLine("    ( o o )");
      Console.WriteLine("    |  O  |");
    }

    static void GhostBody()
    {
      Console.WriteLine("    |     |");
      Console.WriteLine("    |     |");
      Console.WriteLine("    |     |");
    }

    static void GhostFeet()
    {
      Console.WriteLine("    |     |");
      Console.WriteLine("    |     |");
      Console.WriteLine("    '~~~~~'");
    }

    static void BugHead()
    {
      Console.WriteLine("     /   \\");
      Console.WriteLine("     \\. ./");
      Console.WriteLine("    (o + o)");
    }

    static void BugBody()
    {
      Console.WriteLine("  --|  |  |--");
      Console.WriteLine("  --|  |  |--");
      Console.WriteLine("  --|  |  |--");
    }

    static void BugFeet()
    {
      Console.WriteLine("     v   v");
      Console.WriteLine("     *****");
    }

    static void MonsterHead()
    {
      Console.WriteLine("     _____");
      Console.WriteLine(" .-,;='';_),-.");
      Console.WriteLine("  \\_\\(),()/_/");
      Console.WriteLine("　  (,___,)");
    }

    static void MonsterBody()
    {
      Console.WriteLine("   ,-/`~`\\-,___");
      Console.WriteLine("  / /).:.('--._)");
      Console.WriteLine(" {_[ (_,_)");
    }

    static void MonsterFeet()
    {
      Console.WriteLine("    |  Y  |");
      Console.WriteLine("   /   |   \\");
      Console.WriteLine("   \"\"\"\" \"\"\"\"");
    }
  }
}
