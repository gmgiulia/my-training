using System;

namespace ArchitectArithmetic
/* This program calculates the material cost for any architect’s floor plan. 
Author: Giulia Meniconzi*/
{
  class Program
  {
    public static void Main(string[] args)
    {
    
    double rect=AreaRectangle(2500,1500);
    Console.WriteLine("The area of the rectangle is "+rect);
      double circle=AreaCircle(375);
      Console.WriteLine("The area of the circle is "+circle);
      double triangle=AreaTriangle(750, 500);
      Console.WriteLine("The area of the triangle is "+triangle);
      double totalArea=rect+circle+triangle;
      Console.WriteLine("Total area is: " + totalArea);
      double costMaterial=totalArea*180;
      costMaterial=Math.Floor(costMaterial);
      Console.WriteLine("The cost of the flooring material for the total area is "+ costMaterial+ "pesos.");
    }

    
    static double AreaRectangle(double length, double width)
    {
      return length*width;
      
    }

    static double AreaCircle(double radius)
    
    {
     return Math.PI*(Math.Pow(radius,2));
     
    }

    static  double AreaTriangle(double bottom, double height)
    {
     return 0.5*bottom*height;
      
    }
	}
}
