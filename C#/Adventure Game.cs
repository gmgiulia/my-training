using System;

namespace ChooseYourOwnAdventure
{
  class Program
  {
      static void Main(string[] args)
    {
      /* THE MYSTERIOUS NOISE 
      Author Giulia Meniconzi*/
        

      // Start by asking for the user's name:
        Console.Write("What is your name?: ");
        string name = Console.ReadLine();
        Console.WriteLine($"Hello, {name}! Welcome to our story.");
        Console.WriteLine("It begins on a cold rainy night. You're sitting in your room and hear a noise coming from down the hall. Do you go investigate?\nType YES or NO");
        string noiseChoice=Console.ReadLine();
        noiseChoice=noiseChoice.ToUpper();
        if(noiseChoice=="NO")
        {
          Console.WriteLine("Not much of an adventure if we don't leave our room!\nTHE END.");
        }
        else if(noiseChoice=="YES")
        {
          Console.WriteLine("You walk into the hallway and see a light coming from under a door down the hall.\nYou walk towards it. Do you open it or knock?\nType OPEN or KNOCK: ");
        string doorChoice=Console.ReadLine();
        doorChoice=doorChoice.ToUpper();
          if(doorChoice=="KNOCK")
          {
            Console.WriteLine("A voice behind the door speaks. \nIt says, \"Answer this riddle: \n
\"Poor people have it. Rich people need it. If you eat it you die. What is it?\"\nType your answer:");
            string riddleAnswer=Console.ReadLine();
            riddleAnswer=riddleAnswer.ToUpper();
            if(riddleAnswer=="NOTHING")
            {
             Console.WriteLine("The door opens and NOTHING is there.\nYou turn off the light and run back to your room and lock the door.\nTHE END."); 
            }
            else
            {
              Console.WriteLine("You answered incorrectly. The door doesn't open.\nTHE END.");
            }
          }
          else if(doorChoice=="OPEN")
          {
           Console.WriteLine("The door is locked!\nSee if one of your three keys will open it.\nEnter a number between 1 and 3:");
            string keyChoice=Console.ReadLine();
            keyChoice=keyChoice.ToUpper();
            switch(keyChoice)
            {
            case "1":
            Console.WriteLine("You choose the first key. Lucky choice!\nThe door opens and NOTHING is there. Strange...\nTHE END.");
            break;
            case "2":
            Console.WriteLine("You choose the second key.\nThe door doesn't open.\nTHE END.");
            break;
            case "3":
            Console.WriteLine("You choose the third key.\nThe door does open.\nThere is another you of the future with a message for you.\nDo you want to read the message?\nType YES or NO");
            string readMessage=Console.ReadLine();
            readMessage=readMessage.ToUpper();
            if(readMessage=="YES")
            {
             Console.WriteLine("You will be rich.\nGo and buy a ticket for the lottery!THE END"); 
            }
            else if(readMessage=="NO")
            {
              Console.WriteLine("You lost an opportunity.\nI guessed you were scared of knowing what the future holds.\n THE END.");
            }
            break;
            }
          }
        }
       

     
    }
  }
}


