using System;

namespace MadLibs
{
  class Program
  {
    static void Main(string[] args)
    {
      /*
      This program  write a Mad Libs word game! Mad Libs are short stories with blanks for the player to fill in that represent different parts of speech. The end result is a really hilarious and strange story.
      Author: Giulia Meniconzi
      */


      // Let the user know that the program is starting:
      Console.WriteLine("Mad Libs is starting...");

      // Give the Mad Lib a title:
      string title = "Enter a Title for your story";
      Console.WriteLine(title);
      string userTitle=Console.ReadLine();
      
      // Define user input and variables:
      Console.WriteLine("Enter the name of the main character:  ");
      string name=Console.ReadLine();
      Console.WriteLine("\n");
      Console.WriteLine("Now enter an adjective:  ");
      string adj1=Console.ReadLine();
      Console.WriteLine("\n");
      Console.WriteLine("another one...  ");
      string adj2=Console.ReadLine();
      Console.WriteLine("\n");
      Console.WriteLine("OK, now enter the last one:   ");
      string adj3=Console.ReadLine();
      Console.WriteLine("\n");
      Console.WriteLine("Well done! \n Now enter a verb!");
      string verb=Console.ReadLine();
      Console.WriteLine("\n");
      Console.WriteLine("Almost done.\n Please give me a noun:  ");
      string noun=Console.ReadLine();
      Console.WriteLine("\n");
      Console.WriteLine("Another one?   ");
      string noun2=Console.ReadLine();
      Console.WriteLine("\n");
      Console.WriteLine("Last thing before we finish... \n Choose one of this: \n");
      Console.WriteLine("An animal:  ");
      string animal=Console.ReadLine();
      Console.WriteLine("\n");
      Console.WriteLine("A food: ");
      string food=Console.ReadLine();
      Console.WriteLine("\n");
      Console.WriteLine("A fruit: ");
      string fruit=Console.ReadLine();
      Console.WriteLine("\n");
      Console.WriteLine("A superhero:  ");
      string hero=Console.ReadLine();
      Console.WriteLine("\n");
      Console.WriteLine("A country:  ");
      string country=Console.ReadLine();
      Console.WriteLine("\n");
      Console.WriteLine("A dessert:  ");
      string dessert=Console.ReadLine();
      Console.WriteLine("\n");
      Console.WriteLine("A year:  ");
      string year=Console.ReadLine();
      Console.WriteLine("\n");
      Console.WriteLine("\n");

      // The template for the story:

      string story = $"This morning {name} woke up feeling {adj1}. \n'It is going to be a {adj2} day!'\n Outside, a bunch of {animal}s were protesting to keep {food} in stores. They began to {verb} to the rhythm of the {noun}, which made all the {fruit}s very {adj3}.\n Concerned, {name} texted {hero}, who flew {name} to {country} and dropped {name} in a puddle of frozen {dessert}. \n{name} woke up in the year {year}, in a world where {noun2}s ruled the world.";


      // Print the story:
      Console.WriteLine(story);

    }
  }
}
