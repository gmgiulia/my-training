using System;

namespace ArchitectArithmetic
/* This program calculates the material cost for any architect’s floor plan. 
Author: Giulia Meniconzi*/
{
  class Program
  {
    public static void Main(string[] args)
    {
    Console.WriteLine(CalculateTotalCost());
    }


    static string CalculateTotalCost()
    {
        Console.WriteLine("Enter the length of the rectangle: ");
        double length=Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("Enter the width of the rectangle: ");
        double width=Convert.ToDouble(Console.ReadLine());
        double AreaRectangle=length*width;
        Console.WriteLine("Enter the bottom of the triangle: ");
        double bottom=Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("Enter the height of the triangle: ");
        double height=Convert.ToDouble(Console.ReadLine());
        double AreaTriangle=0.5*bottom*height;
        Console.WriteLine("Enter the radius of the circle: ");
        double radius=Convert.ToDouble(Console.ReadLine());
        double AreaCircle=Math.PI*(Math.Pow(radius, 2));
        double totalArea=(AreaRectangle+AreaTriangle+AreaCircle);
        Console.WriteLine("Enter the cost of the flooring material: ");
        double cost=Convert.ToDouble(Console.ReadLine());
        double total=totalArea*180;
        total=Math.Floor(total);
        return $"The cost of the flooring material for the total area is "+ total;
    }
  }
}
