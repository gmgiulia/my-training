using System;

namespace MoneyMaker
{
  class MainClass
  {
    public static void Main(string[] args)
    { /* Money Maker is a program that fine the minimum number of coins that equals n cents*/
      Console.WriteLine("Welcome to Money Maker!");
      Console.WriteLine("What's the amount you would like to convert?\n");
      double amount=Convert.ToDouble(Console.ReadLine());
      amount=Math.Floor(amount);// round down input to the nearest whole cent.
      Console.WriteLine($"{amount} cents is equal to...");
      
      /* Convert to coins: 
      A gold coin is worth 10 cents
			A silver coin is worth 5 cents
      A bronze coin is worth 1 cent */
      //let's define the variables
      int gold=10;
      int silver=5;
      int bronze=1;
      //find the max num of gold coins
      double goldCoins=Math.Floor(amount/gold);
      double remaining=(amount%gold);
      //find the max num of silver coins
      double silverCoins=Math.Floor(remaining/silver);
      remaining=(remaining%silver);
      //Now let's print out all the coins
      Console.WriteLine($"Gold coins: " + goldCoins+ "\nSilver coins: "+ silverCoins  + "\nBronze coins: " + remaining);
      
    }
  }
}
