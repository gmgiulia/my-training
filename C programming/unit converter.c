#include <stdio.h>

double gramToLbs(double grams); //prototype of function
double meteToFeet(double mt);
double celsToFahr(double cels);
int conv;

int main(void){
    double value;
    char unit;
    scanf("%d", &conv); //numbers of conversions
    for(int i=0; i<conv;i++){
        scanf("%lf %c", &value, &unit); //scan number and unit
        if(unit=='m'){ // m for meter
            printf("%.6lf ft\n", meteToFeet(value));
        } else if(unit=='g'){ //g for grams
            printf("%.6lf lbs\n", gramToLbs(value));
        } else { //c for celsius
            printf("%.6lf f\n", celsToFahr(value));
        }
    }
    return 0;
}

double gramToLbs(double grams){ //convert grams to pounds
    double poundToGram= 0.002205; 
    return grams*poundToGram;
}

double meteToFeet(double mt){ //convert meters to feet
    double feetToMete=3.2808;
    return mt*feetToMete;
}

double celsToFahr(double cels){ //convert celsius to fahrenheit
    double fahrToCels=32 ;
    double zerofahr=1.8 ;
    return fahrToCels + zerofahr*cels;
}
