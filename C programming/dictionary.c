// Implements a dictionary's functionality
#include <stdio.h>
#include <stdbool.h>
#include <cs50.h>
#include <string.h>
#include <ctype.h>

#include "dictionary.h"

// Represents number of buckets in a hash table
#define N 26

// Represents a node in a hash table
typedef struct node
{
    char word[LENGTH + 1];
    struct node *next;
}
node;


//Hash prototype
unsigned long hash(char *str);

//Set pointer to hash table
node *hashtable[N];

//Counter for the words in size function
int word_count = 0;


// Loads dictionary into memory, returning true if successful else false
bool load(const char *dictionary)
{
    // Initialize hash table
    for (int i = 0; i < N; i++)
    {
        hashtable[i] = NULL;
    }

    //Open dictionary to read only
    FILE *file = fopen(dictionary, "r");
    if (file == NULL)
    {
        fprintf(stderr, "Could not open file %s.\n", dictionary);
        unload();
        return false;
    }

    // Buffer to store output word
    char word[LENGTH + 1];
    int n = LENGTH + 2;

    // loop through the dictionary until a null character
    while (fgets(word, n, file) != NULL)
    {
        // add null terminator to the end of the word
        word[strlen(word) - 1] = '\0';

        //hash the word
        int index = hash(word) % N;
        //create a new node
        node *new_node = malloc(sizeof(node));
        //Test to see if node is null
        if (new_node == NULL)
        {
            fclose(file);
            return false;

        }

        //cp word into node
        strcpy(new_node -> word, word);
        //insert node into linked list
        new_node -> next = hashtable[index];

        hashtable[index] = new_node;
        word_count++;
    }

    // close the file
    fclose(file);
    return true;
}

// Returns number of words in dictionary if loaded else 0 if not yet loaded
unsigned int size(void)
{
    return word_count;
}

// Returns true if word is in dictionary else false
bool check(const char *word)
{
    //Convert to lowercase and add 1 for null terminator
    int length = strlen(word);
    char copy[length + 1];

    //Add null terminator to end of the lower case word
    copy[length] = '\0';

    for (int i = 0; i < length; i++)
    {
        copy[i] = tolower(word[i]);
    }

    //Pass lower case word to hash function to get index
    int index = hash(copy) % N;

    //Set to the head of the linked list
    node *head = hashtable[index];

    if (head != NULL)
    {
        //Point cursor to location of head
        node *cursor = head;

        //Traverse the linked list
        while (cursor != NULL)
        {
            if (strcmp(copy, cursor->word) == 0)
            {
                //Return true if word matches the word in our dictionary
                return true;
            }

            //Else move cursor to the next linked list
            cursor = cursor->next;
        }
    }

    return false;
}


// Unloads dictionary from memory, returning true if successful else false
bool unload(void)
{
    // for each node in the hashtable
    for (int i = 0; i < N; i++)
    {
        // check the table for a node at that index
        node *cursor = hashtable[i];
        while (cursor != NULL)
        {
            // create a temporary node
            node *new_node = cursor;
            cursor = cursor -> next;

            // free the current node
            free(new_node);
        }
    }
    return true;
}



//Hash table function from https://stackoverflow.com/questions/7666509/hash-function-for-string
unsigned long hash(char *str)
{
    unsigned long hash = 5381;
    int c;

    while ((c = *str++))
    {
        /* hash * 33 + c */
        hash = ((hash << 5) + hash) + c;

    }
    return hash;
}