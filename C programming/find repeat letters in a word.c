#include<stdio.h>
int main(void){
    char word[51];
    char swap;
    int i;
    int j=0;
    int repeats=0;;
    int length=0;
    
    //scan the word 
    scanf("%s", word);
    
    //find the length of the word 
    while(word[length]!='\0')
        length++;
    
    
    //sort the letter alphabetically
    for(j=0; j<length-1; j++){
        for(i=0; i<length-1; i++){
            if(word[i]>word[i+1]){
                swap=word[i];
                word[i]=word[i+1];
                word[i+1]=swap;
            }
        }
    }
    i=0;
    //find a repeat letter in the word
    while (i<length-1) {
        if (word[i]==word[i+1]) {
            repeats++;
            j=i+2;

            //Continues through the word until it reaches a new character
            while (j<length && word[j]==word[i]) 
                j++;
            i = j;
        } else {
            i++;
        }
    }
    
    printf("%d", repeats);
    
   return 0;
}