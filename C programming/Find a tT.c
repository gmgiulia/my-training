#include <stdio.h>
int main(void){
    char word[51];
    int i=0;
    int length=0;
    int halfLength=0;
    int found=0;
    
    scanf("%s", word); //user input for the word
    while(word[length] !='\0'){
        length++; //it counts the number of letters in the word
    }
    if(length%2==0) //if even number
    {halfLength=length/2-1;}
    else { //if odd number
        halfLength=length/2;
    }
    while(!found && i<length){ // loop to find t/T in the word
        if(word[i]=='t' || word[i]=='T'){
            found++;
        } else {i++;}
    } 
    if(!found){
        printf("Not found");
    }
    else if(i<=halfLength){
        printf("Found in the first half");
    } else {
        printf("Found in the second half");
    }
    
    return 0;
}