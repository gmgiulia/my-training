#include <stdio.h>
int sumOfDigits(int); //prototype of function


int main(void){
    int N, result;
    scanf("%d",&N);
    result=sumOfDigits(N);
    printf("%d", result);
    
    return 0;
}

int sumOfDigits(int N){
    if (N<10) //whether it is examining the last digit 
	    { return N;}
	else  
	    { return (N%10 + sumOfDigits(N/10)); } //to isolate value of each digit use modulus operator 
    
   
}