include <stdio.h>
#include <stdbool.h>



int main(int argc, char *argv[])
{

    if (argc != 2)
    {

        fprintf(stderr, "You must write only one command-line!\n");
        return 1;
    }

    //remember forensic image name
    char *forensic = argv[1];

    // open memorycard file
    FILE *file_raw = fopen(forensic, "r");
    if (file_raw == NULL)
    {
        fprintf(stderr, "Could not open %s.\n", forensic);
        return 2;
    }

    //create  variables
    FILE *img = NULL;
    unsigned char buffer[512];
    char filename[8];
    int orderFound = 0;
    int True = 0;

    //Continue read 512 bytes until EOF
    fread(buffer, 512, 1, file_raw);


        //find beginning of jpeg
        if (buffer[0] == 0xff && buffer[1] == 0xd8 && buffer [2] == 0xff && (buffer[3] & (0xf0 == 0xe0)))
        {
            if (True == 1)
            {
                //close jpeg to start new one
                fclose(img);
            }
            else
            {
                True = 1;
            }



            //making a new jpeg
            sprintf(filename, "%03i.jpeg", orderFound);


            //open jpeg found

            img = fopen(filename, "w");
            orderFound++;

        }

        if (True == 1)
        {
            //write 512 bytes until new jpeg is found
            fwrite(&buffer, 512, 1, img);

        }

    //close file
    fclose(file_raw);
    fclose(img);


    //Success
    return 0;
}
