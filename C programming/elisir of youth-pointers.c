#include <stdio.h>  //elixir of youth with pointers
void elixir(int *);


int main(void){
	int age;
	int *ageAddr = &age;
	scanf("%d", ageAddr);
	printf("Your current age is %d.\n", age);
	
	elixir(&age); //call the function elixir

	printf("Your new age will be %d!\n", age);
	return 0;
}

void elixir(int *age){ 
   if(*age>20){ //if >20 become 10 years younger
       *age=*age-10;
   } else { //if <21 double the age
       *age=*age*2;
   }
}

