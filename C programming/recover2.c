#include <stdio.h>
#include <stdbool.h>

int main(int argc, char *argv[])
{

    if (argc != 2)
    {
        fprintf(stderr, "You must write a file name.\n");
        return 1;
    }


    //Open the file
    FILE *raw_file = fopen(argv[1], "r");
    if (raw_file == NULL)
    {
        fprintf(stderr, "Could not open %s.\n", argv[1]);
        return 2;
    }

    // Create outfile for picture
    FILE *img = NULL;

    // Create variables
    unsigned char buffer[512];
    char filename[8];
    int counter = 0;
    bool found = false;

    //Read the file
    while (fread(buffer, 512, 1, raw_file) == 1)
    {
        //Check if we are at the beginning of a JPEG
        if (buffer[0] == 0xff && buffer[1] == 0xd8 && buffer[2] == 0xff && (buffer[3] & 0xf0) == 0xe0)
        {
            //Close current JPEG, so we can start reading the next
            if (found == true)
            {
                fclose(img);
            }
            //Condition for found JPEG
            else
            {
                found = true;
            }

            sprintf(filename, "%03i.jpg", counter);
            img = fopen(filename, "w");
            counter++;
        }

        if (found == true)
        {
            fwrite(&buffer, 512, 1, img);
        }
    }

    //Close all files
    fclose(raw_file);
    fclose(img);

    //Success
    return 0;

}