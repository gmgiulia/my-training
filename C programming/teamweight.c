#include <stdio.h>
int main(void){
    int members, i, weighteven, weightodd, sumeven, sumodd;
    sumeven=0;
    sumodd=0;
    scanf("%d", &members);
    for(i=0; i<members; i++){
        scanf("%d", &weighteven);
        sumeven=sumeven+weighteven;
        scanf("%d", &weightodd);
        sumodd=sumodd+weightodd;
    } 
    if(sumeven>sumodd){
        printf("Team 1 has an advantage\n");
    } else {
        printf("Team 2 has an advantage\n");
    }
    printf("Total weight for team 1: %d\n", sumeven);
    printf("Total weight for team 2: %d\n", sumodd);
    return 0;
    
    
}