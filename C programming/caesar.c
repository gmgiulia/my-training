#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <cs50.h>


int main(int argc, string argv[])
{
// check no. or arguments given make sure there is only 1
    if (argc == 2)
    {
        // count the length of the word function in string.h
        int lengthk = strlen(argv[1]);
        //iterate over each char knowing length
        for (int i = 0; i < lengthk; ++i)
        {
            if (argv[1][i] < 48 || argv[1][i] > 57)
                //if it is in the rage of 48 - 57. IF it is, then it means it is a digit 0-9
            {
                printf("Usage: ./caesar key\n");
                return 1;
            }
            else
            {
        
                //atoi converts string into int
                int key = atoi(argv[1]);
            } 
        }
        printf("Success\n"); 
   
        string plaintext = get_string("plaintext: ");
        int lengthp = strlen(plaintext);
        printf("ciphertext: ");
    
        //iterate over every character in the plaintext 
        for (int i = 0; i < lengthp; i++)
        {
            int key = atoi(argv[1]);
        
            //c = (p + k) % 26
            //Encrypt lower case A=97 a =65
            if islower(plaintext[i])
            {
                printf("%c", (((plaintext[i] + key) - 97) % 26) + 97);
            }
            //encrypt upper case
            else if isupper(plaintext[i])
            {
                printf("%c", (((plaintext[i] + key) - 65) % 26) + 65);
            }
            //print anything else
            else
            {
                printf("%c", plaintext[i]);
            }
        }
        printf("\n");
    
     
    
    }
    else
    {
        printf("Usage: ./caesar key\n");
    }
    return 0;
}