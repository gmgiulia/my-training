#include <stdio.h>

void behind(int *, int);

int main(void) {
    int array[10];
    int N, i;
    
    scanf("%d", &N); //players
    for (i=0; i<N; i++) {
        scanf("%d", &array[i]); //their score
    }
    behind(array, N);
    for (i=0; i<N; i++) { //print how behind are the single players
        printf("%d\n", array[i]);
    }
    
    return 0;
}


void behind(int *score, int players){
    int i;
    int maxscore=0;
   for(i=0; i<players; i++){
       if (score[i]>maxscore){
           maxscore=score[i]; //find the highest score
       }
     }
     for(i=0; i<players; i++){
    score[i]=maxscore-score[i]; //find how points behind are the single players
    
     }
}
    