from cs50 import get_int, get_string


def main():

    while True:
        # ask for credit numb
        Num = get_int("Enter the credit card number: ")
        # convert to string and count lenght
        card = str(Num)
        creditNum = len(str(Num))
        # reject if not 13/15/16 numbers
        if creditNum != 13 and creditNum != 15 and creditNum != 16:
            print("INVALID")
            break
        else:
            break

    sum = 0
    j = 1

    # Luhn’s Algorithm
    # Multiply every other digit by 2 starting with the number’s second-to-last digit
    for i in range(creditNum-1, 0):
        if j % 2 == 0:
            # add those products’ digits together.
            sum += int(card[i])*2 % 10
            if int(card[i])*2 >= 10:
                sum += 1

        else:
            # Add the sum to the sum of the non-multiplied digits
            sum += int(card[i])

    j += 1

    # print name of card AMEX 34/37. MASTER 51 52 53 54 55. VISA 4
    if int(card[0]) == 3 and int(card[1]) == 4 or int(card[1]) == 7 and sum % 10 == 0:
        print("AMEX")
    elif int(card[0]) == 5 and int(card[1]) > 0 and int(card[1]) < 6 and sum % 10 == 0:
        print("MASTERCARD")
    elif int(card[0]) == 4 and sum % 10 == 0:
        print("VISA")
    else:
        print("INVALID")


if __name__ == "__main__":
    main()