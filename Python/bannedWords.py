from sys import argv
from cs50 import get_string, get_int


def main():

    # sole command-line argument the name of a dictionary
    if len(argv) != 2:
        print("INVALID")
        exit(1)

    else:
        dictionary = argv[1]
        file = open(dictionary, "r")
        # stores each word in a Python data structure
        ban = set()
        for line in file:
            ban.add(line.strip())
        file.close()
        print("What message would you like to censor?")
        message = get_string("")
        token = set()
        # Tokenizes that message into its individual component words
        token = message.split()
        # iterate over words in token
        for word in token:
            # check if same word
            if word.lower() in ban:
                for c in word:
                    print("*", end="")
                print(" ", end="")

            else:
                print(word, end=" ")
        print()


if __name__ == "__main__":
    main()
