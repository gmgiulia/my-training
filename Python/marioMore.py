from cs50 import get_int


def main():
    while True:
      # user input and reject if not between 1 and 8
        height = get_int("Height: ")
        if height <= 0 or height >= 9:
            height
        else:
          # make pyramid
            for i in range(0, height):
                hashes = i + 1
                spaces = height - hashes
                # print pyramid
                print(" " * spaces, end="")
                print("#" * hashes, end="")
                print("  ", end="")
                print("#" * hashes)
                # exit from the program
            break


if __name__ == "__main__":
    main()
