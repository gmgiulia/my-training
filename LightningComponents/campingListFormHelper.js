({
	createItem : function(component, event, helper) {
        var isFormValid = component.find("campingItemForm").reduce(function(isValid, inputCmp){
        	inputCmp.showHelpMessageIfInvalid();    	
            return isValid && inputCmp.get("v.validity").valid;
        });
        
        if (isFormValid) {
            var createItem = component.getItem("createItem");
    		createItem.setParams({ "item": item });
            createItem.fire();
            component.set("v.newItem",{ 'sobjectType': 'Camping_Item__c',                    'Name': '',
                    'Quantity__c': 0,
                    'Price__c': 0,
                    'Packed__c': false }); 
        }
	},
    
})