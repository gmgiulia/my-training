({
        handleCreateExpense: function(component, event, helper) {
        var newExpense = event.getParam("expense");
        helper.createExpense(component, newExpense);
    },

    
        handleUpdateExpense: function(component, event, helper) {
        var updatedExp = event.getParam("expense");
        helper.updateExpense(component, updatedExp);
    },

    
        // Load expenses from Salesforce
    	doInit: function(component, event, helper) {
        // Create the action
        var action = component.get("c.getExpenses");
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.expenses", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        // Send action off to be executed
        $A.enqueueAction(action);
    },
    
    
    clickReimbursed: function(component, event, helper) {
        var expense = component.get("v.expense");
        //Creates an event named updateExpense
        var updateEvent = component.getEvent("updateExpense");
        //Packages expense into the event, set data to be passed to the server-side controller
        updateEvent.setParams({ "expense": expense });
        updateEvent.fire();
    }
})
